package ba.elvedin.symphonygallery.repository

import android.content.Context
import android.database.Cursor
import android.database.MergeCursor
import android.graphics.Bitmap
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import androidx.core.net.toUri
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import ba.elvedin.symphonygallery.R
import ba.elvedin.symphonygallery.database.ImagesDatabase
import ba.elvedin.symphonygallery.database.asDomainModelList
import ba.elvedin.symphonygallery.database.asDomainModelSingle
import ba.elvedin.symphonygallery.domain.ImageDomainModel
import ba.elvedin.symphonygallery.domain.asDbModelList
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import timber.log.Timber
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream
import java.lang.Exception
import android.content.Intent
import ba.elvedin.symphonygallery.database.ImageDbModel
import kotlin.collections.ArrayList



class ImagesRepository(private val database: ImagesDatabase,private val context: Context){
    val allImages: LiveData<List<ImageDomainModel>> = Transformations.map(database.imagesDao.getAllImagePaths()){
        it.asDomainModelList()
    }

    fun getSingleImagePath(id: String): LiveData<ImageDomainModel>{
        return Transformations.map(database.imagesDao.getSingleImage(id)){it.asDomainModelSingle()}
    }

    suspend fun refreshImages(){
        withContext(Dispatchers.IO){
            System.out.println("REFRESHING IMAGES")
            Timber.d("Refreshing images")
            val images = getAllImagesFromStorage()
            System.out.println("IMAGES: " + images.size)
            database.imagesDao.insertAll(images.asDbModelList())
        }
    }

    suspend fun downloadImage(url: String, imageName: String){
        withContext(Dispatchers.IO) {
            val imgUri = url.toUri().buildUpon().scheme("https").build()
            saveImage(Glide.with(context)
                .asBitmap()
                .load(imgUri)
                .apply(RequestOptions().placeholder(R.drawable.loading_animation)
                    .error(R.drawable.ic_broken_image))
                .submit().get(), imageName
            )
        }
        refreshImages()
    }

    private fun saveImage(image: Bitmap, imageName: String) {
        val imageFileName = "JPEG_$imageName.jpg"
        //
//        val storageDirectory = File(context.getExternalFilesDir(Environment.DIRECTORY_DCIM)!!.toString()+"/Camera/")
        val storageDirectory = File("/storage/emulated/0/DCIM/Camera/")

        var success = true
        if(!storageDirectory.exists()){
            success = storageDirectory.mkdirs()
        }
        val imageFile=File(storageDirectory, imageFileName)
        if(success){
            try {
                val fOut: OutputStream = FileOutputStream(imageFile)
                image.compress(Bitmap.CompressFormat.JPEG, 100, fOut)
                fOut.close()
                println("NEW IMAGE PATH: " + imageFile.absolutePath)
                database.imagesDao.insertSingleImage(ImageDbModel(imageFile.absolutePath))
                context.sendBroadcast(Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE, Uri.fromFile(imageFile)))
            }catch (e: Exception){
                e.printStackTrace()
            }
        }
    }

    private fun getAllImagesFromStorage(): List<ImageDomainModel>{
        val images = ArrayList<ImageDomainModel>()
        val uriExternal: Uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
        val uriInternal: Uri = MediaStore.Images.Media.INTERNAL_CONTENT_URI
        val cursorExternal: Cursor?
        val cursorInternal: Cursor?

        val projection = arrayOf(
            MediaStore.Images.Media.DATA,
        )

        cursorExternal = context.contentResolver.query(uriExternal, projection, null, null,null)
        cursorInternal = context.contentResolver.query(uriInternal, projection, null, null,null)
        val cursor = MergeCursor(arrayOf(cursorExternal, cursorInternal))

        cursor.use {
            it.let {
                val pathColumn = it.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)

                while (it.moveToNext()) {
                    val imgPath = it.getString(pathColumn)
                    println("IMAGE PATH: $imgPath")
                    images.add(ImageDomainModel(imgPath))
                }
            }
        }
        return images
    }
}