package ba.elvedin.symphonygallery.domain

import ba.elvedin.symphonygallery.database.ImageDbModel
import java.util.*

data class ImageDomainModel(val imgPath: String)

fun List<ImageDomainModel>.asDbModelList(): List<ImageDbModel>{
    return map{
        ImageDbModel( imgPath = it.imgPath)
    }
}

fun ImageDomainModel.asDomainModelSingle(): ImageDbModel{
    return ImageDbModel(imgPath)
}