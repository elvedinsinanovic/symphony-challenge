package ba.elvedin.symphonygallery.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import ba.elvedin.symphonygallery.database.getDatabase
import ba.elvedin.symphonygallery.domain.ImageDomainModel
import ba.elvedin.symphonygallery.repository.ImagesRepository
import kotlinx.coroutines.launch
import timber.log.Timber

enum class ImagesLoadingStatus { LOADING, ERROR, DONE }

class GalleryViewModel (application: Application): AndroidViewModel(application){
    private val imagesRepository = ImagesRepository(getDatabase(application), application.applicationContext)

    private val _status = MutableLiveData<ImagesLoadingStatus>()

    val status: LiveData<ImagesLoadingStatus>
        get() = _status

    private var _images = imagesRepository.allImages

    val images: LiveData<List<ImageDomainModel>>
        get() = _images

    private val _navigateToSelectedImage = MutableLiveData<ImageDomainModel>()
    val navigateToSelectedImage: LiveData<ImageDomainModel>
        get() = _navigateToSelectedImage

    private val _navigateToAddImage = MutableLiveData<Boolean>()
    val navigateToAddImage: LiveData<Boolean>
        get() = _navigateToAddImage

    init {
        getAllImages()
    }

    private fun getAllImages() {
        viewModelScope.launch {
            try {
                Timber.d("Getting all images")
                imagesRepository.refreshImages()
                _status.value = ImagesLoadingStatus.DONE
            } catch (e: Exception) {
                _status.value = ImagesLoadingStatus.ERROR
            }
        }
    }

    fun startAddImage(){
        _navigateToAddImage.value=true
    }

    fun addImageNavigationComplete(){
        _navigateToAddImage.value=false
    }

    fun displaySingleImage(image: ImageDomainModel){
        _navigateToSelectedImage.value = image
    }

    fun displaySingleImageComplete(){
        _navigateToSelectedImage.value = null
    }

}