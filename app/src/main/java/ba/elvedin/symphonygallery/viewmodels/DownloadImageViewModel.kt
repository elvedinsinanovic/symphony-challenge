package ba.elvedin.symphonygallery.viewmodels

import android.app.Application
import androidx.databinding.Bindable
import androidx.lifecycle.*
import ba.elvedin.symphonygallery.GalleryApplication
import ba.elvedin.symphonygallery.database.getDatabase
import ba.elvedin.symphonygallery.repository.ImagesRepository
import kotlinx.coroutines.launch

class DownloadImageViewModel(application: Application): AndroidViewModel(application){
    private val imagesRepository = ImagesRepository(getDatabase(application), application)

    val _imageUrl = MutableLiveData<String>()

    var imageUrl : LiveData<String>
        get() = _imageUrl
        set(value) {
            _imageUrl.value =value.value
        }

    init {
        _imageUrl.value=""
    }

    fun startDownloadingImage(url: String){
        println("Entered URL in ViewModel: " + _imageUrl.value.toString())
        viewModelScope.launch {
            url.let {
                imagesRepository.downloadImage(url, url.substring(url.lastIndexOf("/")+1))
            }
        }
    }
}