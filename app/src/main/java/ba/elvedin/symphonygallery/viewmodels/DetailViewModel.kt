package ba.elvedin.symphonygallery.viewmodels

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import ba.elvedin.symphonygallery.database.getDatabase
import ba.elvedin.symphonygallery.domain.ImageDomainModel
import ba.elvedin.symphonygallery.repository.ImagesRepository

enum class FilterType { NONE, GRAYSCALE, NEGATIVE }

class DetailViewModel(singleImage: String, app: Application) : AndroidViewModel(app) {

    private val imagesRepository = ImagesRepository(getDatabase(app), app.applicationContext)
    private var _selectedImage = MutableLiveData<ImageDomainModel>()

    val selectedImage: LiveData<ImageDomainModel>
        get() = _selectedImage

    private var _filterType = MutableLiveData<FilterType>()

    val filterType: LiveData<FilterType>
        get() = _filterType

    init {
        _filterType.value=FilterType.NONE
        _selectedImage=imagesRepository.getSingleImagePath(singleImage) as MutableLiveData<ImageDomainModel>
    }

    fun updateFilter(filter: FilterType){
        _filterType.value=filter
    }
}