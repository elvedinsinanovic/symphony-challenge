package ba.elvedin.symphonygallery.adapters

import android.graphics.Bitmap
import android.graphics.ColorMatrix
import android.graphics.ColorMatrixColorFilter
import android.net.Uri
import android.view.View
import android.widget.ImageView
import androidx.core.net.toUri
import androidx.databinding.BindingAdapter

import androidx.recyclerview.widget.RecyclerView
import ba.elvedin.symphonygallery.R
import ba.elvedin.symphonygallery.domain.ImageDomainModel
import ba.elvedin.symphonygallery.viewmodels.FilterType
import ba.elvedin.symphonygallery.viewmodels.ImagesLoadingStatus
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import java.io.File
import android.provider.MediaStore
import android.widget.EditText

@BindingAdapter("imageSource", "filterType")
fun ImageView.bindImage(imgSource: String?, filterType: FilterType){
    imgSource?.let {
        val file = File(imgSource)
        val imgUri = Uri.fromFile(file)
        val bitmap = MediaStore.Images.Media.getBitmap(this.context.contentResolver, imgUri)
        Glide.with(this.context)
//            .load(bitmap).apply(RequestOptions.bitmapTransform(GrayscaleTransform(context)))
            .load(imgUri)
            .apply(RequestOptions().placeholder(R.drawable.loading_animation)
                .error(R.drawable.ic_broken_image))
            .into(this)

        when(filterType){
            FilterType.NONE -> return@let
            FilterType.GRAYSCALE -> {
                val colorMatrix= ColorMatrix()
                colorMatrix.setSaturation(0F)
                val colorMatrixFilter= ColorMatrixColorFilter(colorMatrix)
                this.colorFilter = colorMatrixFilter
            }
            FilterType.NEGATIVE -> return@let
        }
    }
}



@BindingAdapter("listData")
fun bindRecyclerView(recyclerView: RecyclerView, data: List<ImageDomainModel>?) {
    val adapter = recyclerView.adapter as PhotoGridAdapter
    adapter.submitList(data)
}

@BindingAdapter("imageLoadingStatus")
fun bindStatus(
    statusImageView: ImageView,
    status: ImagesLoadingStatus?,
) {
    when(status){
        ImagesLoadingStatus.LOADING -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.loading_animation)
        }
        ImagesLoadingStatus.ERROR -> {
            statusImageView.visibility = View.VISIBLE
            statusImageView.setImageResource(R.drawable.ic_broken_image)
        }
        ImagesLoadingStatus.DONE -> {
            statusImageView.visibility = View.GONE
        }
    }
}

