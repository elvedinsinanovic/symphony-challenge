package ba.elvedin.symphonygallery.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import ba.elvedin.symphonygallery.databinding.GridViewItemBinding
import ba.elvedin.symphonygallery.domain.ImageDomainModel

class PhotoGridAdapter(private val onClickListener: OnClickListener): ListAdapter<ImageDomainModel, PhotoGridAdapter.ImageViewHolder>(DiffCallback){

    class ImageViewHolder(private var binding: GridViewItemBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(singleImage: ImageDomainModel){
            binding.image = singleImage
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(GridViewItemBinding.inflate(
            LayoutInflater.from(parent.context)))
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        val singleImage = getItem(position)
        holder.itemView.setOnClickListener{onClickListener.onClick(singleImage)}
        holder.bind(singleImage)
    }
}

class OnClickListener(val clickListener: (singleImage:ImageDomainModel) -> Unit) {
    fun onClick(singleImage:ImageDomainModel) = clickListener(singleImage)
}

object DiffCallback: DiffUtil.ItemCallback<ImageDomainModel>() {
    override fun areItemsTheSame(oldItem: ImageDomainModel, newItem: ImageDomainModel): Boolean {
        return oldItem.imgPath == newItem.imgPath
    }

    override fun areContentsTheSame(oldItem: ImageDomainModel, newItem: ImageDomainModel): Boolean {
        return oldItem == newItem
    }
}