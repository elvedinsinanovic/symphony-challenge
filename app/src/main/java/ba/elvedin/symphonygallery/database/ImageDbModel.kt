package ba.elvedin.symphonygallery.database

import androidx.room.Entity
import androidx.room.PrimaryKey
import ba.elvedin.symphonygallery.domain.ImageDomainModel
import java.util.*

@Entity(tableName = "image_paths_storage")
data class ImageDbModel constructor(
    @PrimaryKey
    val imgPath: String
)

fun List<ImageDbModel>.asDomainModelList(): List<ImageDomainModel>{
    return map{
        ImageDomainModel(imgPath = it.imgPath)
    }
}

fun ImageDbModel.asDomainModelSingle(): ImageDomainModel{
    return ImageDomainModel(imgPath)
}
