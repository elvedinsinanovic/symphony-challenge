package ba.elvedin.symphonygallery.database

import android.content.Context
import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface ImagesDao{
    @Query("SELECT * FROM image_paths_storage")
    fun getAllImagePaths(): LiveData<List<ImageDbModel>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insertAll(allImagePaths: List<ImageDbModel>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertSingleImage(singleImage: ImageDbModel)

    @Query("SELECT * FROM image_paths_storage WHERE imgPath= :key")
    fun getSingleImage(key: String): LiveData<ImageDbModel>
}

@Database(entities = [ImageDbModel::class], version = 6)
abstract class ImagesDatabase: RoomDatabase() {
    abstract val imagesDao: ImagesDao
}

private lateinit var INSTANCE: ImagesDatabase

fun getDatabase(context: Context): ImagesDatabase{
    synchronized(ImagesDatabase::class.java){
        if(!::INSTANCE.isInitialized){
            INSTANCE = Room.databaseBuilder(context.applicationContext,
                ImagesDatabase::class.java, "images").fallbackToDestructiveMigration().build()
        }
    }

    return INSTANCE
}