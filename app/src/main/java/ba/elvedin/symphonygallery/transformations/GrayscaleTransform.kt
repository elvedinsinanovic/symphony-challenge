package ba.elvedin.symphonygallery.transformations

import android.content.Context
import android.graphics.Bitmap
import android.graphics.Color
import android.os.Build
import androidx.core.graphics.blue
import androidx.core.graphics.green
import androidx.core.graphics.red
import com.bumptech.glide.load.engine.bitmap_recycle.BitmapPool
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation

import java.security.MessageDigest
import kotlin.math.roundToInt


class GrayscaleTransform(context: Context) : BitmapTransformation(){

    private val VERSION = 1
    private val ID =
        "ba.elvedin.symphonygallery.transformations.GrayscaleTransform.$VERSION"

    override fun updateDiskCacheKey(messageDigest: MessageDigest) {
        messageDigest.update(ID.toByteArray(CHARSET))
    }

    override fun transform(
        pool: BitmapPool,
        toTransform: Bitmap,
        outWidth: Int,
        outHeight: Int,
    ): Bitmap {
        val width = toTransform.width
        val height = toTransform.height
//Y = 0.21*R + 0.72*G + 0.07*B
        for (x in 0 until width) {
            for (y in 0 until height) {
                val redColor: Int = Color.red(toTransform.getPixel(x,y).red)
                val greenColor: Int= toTransform.getPixel(x,y).green
                val blueColor= toTransform.getPixel(x,y).blue
                val setColor:Float = 0.21F*redColor + 0.72F*greenColor + 0.07F*blueColor
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    toTransform.setPixel(x, y, Color.rgb(setColor, setColor, setColor))
                }else{
                    val intColor=setColor.roundToInt()
                    toTransform.setPixel(x, y, Color.rgb(intColor, intColor, intColor))
                }
            }
        }

        return toTransform
    }

    override fun toString(): String {
        return "GrayscaleTransformation()"
    }

    override fun equals(o: Any?): Boolean {
        return o is GrayscaleTransform
    }

    override fun hashCode(): Int {
        return ID.hashCode()
    }


}