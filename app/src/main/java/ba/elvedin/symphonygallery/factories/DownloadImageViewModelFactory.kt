package ba.elvedin.symphonygallery.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ba.elvedin.symphonygallery.viewmodels.DownloadImageViewModel

class DownloadImageViewModelFactory (
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DownloadImageViewModel::class.java)) {
            return DownloadImageViewModel(application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}