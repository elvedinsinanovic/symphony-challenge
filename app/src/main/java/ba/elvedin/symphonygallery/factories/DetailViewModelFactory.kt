package ba.elvedin.symphonygallery.factories

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ba.elvedin.symphonygallery.viewmodels.DetailViewModel

class DetailViewModelFactory(
    private val imageId: String,
    private val application: Application
) : ViewModelProvider.Factory {
    @Suppress("unchecked_cast")
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(DetailViewModel::class.java)) {
            return DetailViewModel(imageId, application) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}