package ba.elvedin.symphonygallery.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import ba.elvedin.symphonygallery.R
import ba.elvedin.symphonygallery.databinding.FragmentDownloadImageBinding
import ba.elvedin.symphonygallery.factories.DownloadImageViewModelFactory
import ba.elvedin.symphonygallery.viewmodels.DownloadImageViewModel

/**
 * A simple [Fragment] subclass.
 * Use the [DownloadImageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DownloadImageFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        // Inflate the layout for this fragment
        val application = requireNotNull(activity).application
        val binding = FragmentDownloadImageBinding.inflate(inflater)
        binding.lifecycleOwner = this
        val viewModelFactory = DownloadImageViewModelFactory(application)
        val viewModel = ViewModelProvider(this,viewModelFactory).get(DownloadImageViewModel::class.java)
        binding.viewModel= viewModel

        binding.button.setOnClickListener {
            val url = binding.editTextTextPersonName.text.toString()
            println("Entered URL: $url")
            viewModel.startDownloadingImage(url)
        }

        return binding.root
    }
}