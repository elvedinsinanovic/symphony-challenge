package ba.elvedin.symphonygallery.ui.fragments

import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import ba.elvedin.symphonygallery.R
import ba.elvedin.symphonygallery.databinding.FragmentDetailImageBinding
import ba.elvedin.symphonygallery.factories.DetailViewModelFactory
import ba.elvedin.symphonygallery.viewmodels.DetailViewModel
import ba.elvedin.symphonygallery.viewmodels.FilterType


/**
 * A simple [Fragment] subclass.
 * Use the [DetailImageFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class DetailImageFragment : Fragment() {
    lateinit var viewModel: DetailViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        val application = requireNotNull(activity).application
        val binding = FragmentDetailImageBinding.inflate(inflater)
        binding.lifecycleOwner=this
        val imageId = DetailImageFragmentArgs.fromBundle(requireArguments()).selectedImageId
        val viewModelFactory = DetailViewModelFactory(imageId, application)
        viewModel =ViewModelProvider(this, viewModelFactory).get(DetailViewModel::class.java)
        binding.viewModel = viewModel
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
            when(item.itemId){
                R.id.show_histogram -> Toast.makeText(context,"HISTOGRAM!", Toast.LENGTH_SHORT).show()
                R.id.show_negative -> Toast.makeText(context,"NEGATIVE!", Toast.LENGTH_SHORT).show()
                R.id.show_grayscale -> viewModel.updateFilter(FilterType.GRAYSCALE)
            }

        return true
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.image_detail_menu, menu)
        super.onCreateOptionsMenu(menu, inflater)
    }
}