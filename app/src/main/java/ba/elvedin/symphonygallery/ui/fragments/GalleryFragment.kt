package ba.elvedin.symphonygallery.ui.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ba.elvedin.symphonygallery.adapters.OnClickListener
import ba.elvedin.symphonygallery.adapters.PhotoGridAdapter
import ba.elvedin.symphonygallery.databinding.FragmentGaleryBinding
import ba.elvedin.symphonygallery.viewmodels.GalleryViewModel


class GalleryFragment : Fragment() {

    private val viewModel: GalleryViewModel by lazy {
        ViewModelProvider(this).get(GalleryViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        // Inflate the layout for this fragment
        val binding = FragmentGaleryBinding.inflate(inflater)
        binding.lifecycleOwner=this
        binding.viewModel = viewModel

        binding.photosGrid.adapter=PhotoGridAdapter(OnClickListener {
            viewModel.displaySingleImage(it)
        })

        viewModel.navigateToSelectedImage.observe(viewLifecycleOwner, {
            it?.let {
                this.findNavController().navigate(GalleryFragmentDirections.actionGalleryFragmentToDetailImageFragment(it.imgPath))
                viewModel.displaySingleImageComplete()
            }
        })

        viewModel.navigateToAddImage.observe(viewLifecycleOwner,{
            if(it == true){
                this.findNavController().navigate(GalleryFragmentDirections.actionGalleryFragmentToDownloadImageFragment())
                viewModel.addImageNavigationComplete()
            }
        })

        return  binding.root
    }

}